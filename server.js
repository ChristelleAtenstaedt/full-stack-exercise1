// Import the required modules
const http = require('http');

// Define the server's hostname and port
const hostname = '127.0.0.1';
const port = 3003;

// Create the server
const server = http.createServer((req, res) => {
  // Send the response
  res.end('Hello, World! This is a Node.js server.\n');
});

// Start the server and listen for incoming requests
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});